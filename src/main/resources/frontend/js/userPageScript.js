/**
 * 
 */

window.onload = function() {
	console.log('window loaded');
	viewCompletedReim = 'true';
	changeView();
	document.getElementById('reimbursement-btn').addEventListener('click', changeView);
	let welcomeUser = document.getElementById('welcoming');
	welcomeUser.innerText = `Welcome ${getName()}`;	
	sortTable(6);
}



function getReimbursementsOnLoad() {
	console.log('getting reimbursements');

	let btn = document.getElementById('newReimbursementBtn');
	let spinner = document.getElementById('loading-spinner');
	document.getElementById("container-heading").innerText = "Loading...";

	spinner.toggleAttribute("hidden");

	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {

			let reim = JSON.parse(xhttp.responseText);

			btn.innerText = "New Reimbursement";
			spinner.toggleAttribute("hidden");

			buildTable(reim);
		}
	}

	xhttp.open("GET", "http://localhost:9001/ers/userHome/reimbursements");

	xhttp.send();
}


function buildTable(reim) {

	let table = document.getElementById("table-body");

	table.innerHTML = '';

	for (let i = 0; i < reim.length; i++) {

		let temp = verifyValues(reim[i]);

		let row = table.insertRow(i);

		let cell1 = row.insertCell(0);
		let cell2 = row.insertCell(1);
		let cell3 = row.insertCell(2);
		let cell4 = row.insertCell(3);
		let cell5 = row.insertCell(4);
		let cell6 = row.insertCell(5);
		let cell7 = row.insertCell(6);
		let cell8 = row.insertCell(7);
		let cell9 = row.insertCell(8);

		cell1.innerHTML = temp.reim_Id;
		cell2.innerHTML = '$' + temp.reim_amount;
		cell3.innerHTML = temp.reim_type;
		cell4.innerHTML = temp.reim_status;
		cell5.innerHTML = temp.reim_description;
		cell6.innerHTML = temp.reim_submitted;
		cell7.innerHTML = temp.reim_resolved;
		cell8.innerHTML = temp.reim_resolver;
		cell9.innerHTML = temp.reim_receipt;

	}

}


function verifyValues(temp) {
	if (temp.reim_description === null) {
		temp.reim_description = "---";
	}

	if (temp.receipt == null) {
		temp.reim_receipt = "---";
	}

	if (temp.reim_resolver === null) {
		temp.reim_resolver = "---";
	}

	if(!viewCompletedReim){
		temp.reim_resolved = "---";	
	}

	return temp;
}




function changeView() {

	console.log('Updating table');

	let btn = document.getElementById('reimbursement-btn');
		document.getElementById("container-heading").innerText = "Loading...";
	let spinner = document.getElementById('loading-spinner');

	//btn.innerText = "Loading...";
	spinner.toggleAttribute("hidden");

	viewCompletedReim = !viewCompletedReim

	// set table to show open reimbursements
	if (!viewCompletedReim) {

		console.log('Updating reimbursements');

		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {

			if (xhttp.readyState == 4 && xhttp.status == 200) {

				let reim = JSON.parse(xhttp.responseText);

				document.getElementById('container-heading').innerText = 'View Open Reimbursements';
				btn.innerText = "Completed Reimbursements";
				spinner.toggleAttribute("hidden");

				buildTable(reim);
			}
		}

		xhttp.open("GET", "http://localhost:9001/ers/userHome/uncompleted");

		xhttp.send();

	} else {
		//set table to show closed reimbursements
		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {

			if (xhttp.readyState == 4 && xhttp.status == 200) {

				let reim = JSON.parse(xhttp.responseText);

				document.getElementById('container-heading').innerText = 'Completed Reimbursements';
				document.getElementById('table-body').innerHTML = '';

				btn.innerText = "Open Reimbursements";
				spinner.toggleAttribute("hidden");

				buildTable(reim);
			}
		}

		xhttp.open("GET", "http://localhost:9001/ers/userHome/completed");

		xhttp.send();
	}

}






$('#newReim').on('show.bs.modal', function(event) {

	// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

	var modal = $(this)
	modal.find('#newReimForm').attr("action", '/ers/userHome/newReimbursement/')
})