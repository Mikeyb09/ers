package com.ers.controller;

import com.ers.model.User;
import com.ers.service.UserService;

import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ;
	
	public UserController() {
		
	}
	
	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public Handler home = (ctx) -> {
		
		ctx.redirect("/html/index.html");
		
	};
	
	public Handler getLogin = (ctx) -> {
		
//		System.out.println("session: " + ctx.sessionAttribute("userInfo"));
//		System.out.println(java.time.LocalDate.now());  
		
		if(ctx.sessionAttribute("userInfo") == null) {
			ctx.redirect("/html/index.html");			
		} else {
			User user = ctx.sessionAttribute("userInfo");
			
			if(user.getUserRole() == 1) {
				ctx.redirect("/html/managerHome.html");
			} else if(user.getUserRole() == 2){
				ctx.redirect("/html/userHome.html");
			}
		}
	};
	
	
	public Handler postLogin = (ctx) ->{
		
		if(uServ.passwordVerify(ctx.formParam("username"), ctx.formParam("password"))) {
			System.out.println("User is Verified");
			
			if(ctx.sessionAttribute("userInfo") == null) {
				
				User user = uServ.getUserByUsername(ctx.formParam("username"));
				
				ctx.cookie("userId", "" + user.getUserId());
				ctx.cookie("firstName", user.getFirstName());
				ctx.cookie("username", user.getUsername());
				ctx.sessionAttribute("userInfo", user);
				
				if(user.getUserRole() == 1) {
					ctx.redirect("/html/managerHome.html");
				} else if(user.getUserRole() == 2){
					ctx.redirect("/html/userHome.html");
				}
			} else {
				ctx.redirect("/html/index.html");
			}
		}
		
	};
	
	public Handler logout = (ctx) -> {
		ctx.cookie("userId", "");
		ctx.cookie("firstName", "");
		ctx.cookie("username", "");
		ctx.sessionAttribute("userInfo", null);
		ctx.redirect("/html/index.html");
	};
	
	
	public Handler newUser = (ctx) -> {
		
		if(uServ.addNewUser(ctx.formParam("username"), ctx.formParam("password"), ctx.formParam("firstName"), ctx.formParam("lastName"), ctx.formParam("email"))) {
			ctx.redirect("/html/managerHome.html");
		} else {
			throw new NullPointerException();
		}
		
	};
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
