package com.ers.controller;

import java.util.List;

import org.apache.log4j.Logger;

import com.ers.model.Reimbursement;
import com.ers.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {

	private ReimbursementService rServ;
	private Logger log = Logger.getLogger(ReimbursementController.class);

	public ReimbursementController() {

	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}

	/*
	 * MANAGERS HOME VIEW
	 */

	public Handler managerHomeUncompleted = (ctx) -> {

		List<Reimbursement> reimList = rServ.getAllPendingReims();

		log.info(reimList);

		ctx.json(reimList);

	};

	public Handler managerHomeCompleted = (ctx) -> {

		List<Reimbursement> reimList = rServ.getAllCompletedReims();

		log.info(reimList);

		ctx.json(reimList);

	};

	public Handler viewSingleReimbursement = (ctx) -> {

		int reimId = Integer.parseInt(ctx.pathParam("reimId"));

		Reimbursement reim = rServ.getReimById(reimId);

		ctx.json(reim);
	};

	public Handler submitReimChanges = (ctx) -> {

		int reimId = Integer.parseInt(ctx.pathParam("reimId"));
		int userId = Integer.parseInt(ctx.cookie("userId"));
		int statusId = Integer.parseInt(ctx.formParam("status"));

		if (rServ.updateReim(reimId, userId, statusId)) {
			ctx.redirect("/html/index.html");
		} else {
			throw new NullPointerException();
		}

	};

	/*
	 * EMPLOYEE HOME VIEW
	 */

//	public Handler userHomeReimbursement = (ctx) -> {
//
//		List<Reimbursement> reimList = rServ.getReimByAuthor(ctx.cookie("username"));
//
//		log.info(reimList);
//
//		ctx.json(reimList);
//
//	};

	public Handler userHomeUncompleted = (ctx) -> {

		List<Reimbursement> reimList = rServ.getAuthorPendingReim(ctx.cookie("userId"));

		log.info(reimList);

		ctx.json(reimList);

	};

	public Handler userHomeCompleted = (ctx) -> {

		List<Reimbursement> reimList = rServ.getAuthorCompletedReim(ctx.cookie("userId"));

		log.info(reimList);

		ctx.json(reimList);

	};

	public Handler createNewReimbursement = (ctx) -> {

		System.out.println(
				"session: " + ctx.sessionAttributeMap() + "\n" + ctx.cookie("userId") + "\n" + ctx.formParamMap());

		if (rServ.createNewReim(ctx.formParam("amount"), ctx.formParam("description"), ctx.formParam("type"),
				ctx.cookie("userId"))) {
			ctx.redirect("/html/index.html");
		} else {
			System.out.println("Something went wrong....");
			ctx.redirect("/ers/error");
		}

	};

}
