package com.ers.service;

import java.util.ArrayList;
import java.util.List;

import com.ers.dao.ReimbursementDao;
import com.ers.model.Reimbursement;

public class ReimbursementService {
	
	private ReimbursementDao rDao;
	
	public ReimbursementService() {
		
	}
	
	public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}
	
//	TESTED!
	public Reimbursement getReimById(int id) {
		Reimbursement reim = rDao.findById(id);
		
		if(reim == null) {
			throw new NullPointerException();
		}
		
		return reim;
	}
	
//	TESTED!
	public List<Reimbursement> getAllReims(){
		List<Reimbursement> reimList = rDao.findAllReimbursements();
		
		return reimList;		
	} 
	
	public List<Reimbursement> getAllPendingReims(){
		List<Reimbursement> reimList = rDao.findByStatus("Pending");
		
		return reimList;
	}
	
	public List<Reimbursement> getAllCompletedReims(){
		List<Reimbursement> reimList = rDao.findByStatus("");
		
		return reimList;
	}
	
//	TESTED!
//	public List<Reimbursement> getReimByAuthor(String name) {
//		List<Reimbursement> reimList = rDao.findByAuthor(name);
//		
//		return reimList;		
//	}
	
	public List<Reimbursement> getAuthorPendingReim(String userId) {
		List<Reimbursement> reimList = rDao.findByAuthorAndStatus(Integer.parseInt(userId), "Pending");
		
		return reimList;		
	}
	
	public List<Reimbursement> getAuthorCompletedReim(String userId) {
		List<Reimbursement> reimList = rDao.findByAuthorAndStatus(Integer.parseInt(userId), "");
		
		return reimList;		
	}
	
//	MUST UPDATE RESOLVER FIRST
	public List<Reimbursement> getReimByResolver(String name){
		List<Reimbursement> reimList = rDao.findByResolver(name);
		
		return reimList;
	}	
	
	public boolean createNewReim(String strAmount, String description, String type, String strUserId) {
		boolean isVerified = false;
		
		strAmount = strAmount.replaceAll("[^0-9.]", "");		
		description = description.replaceAll("[^a-zA-Z0-9,.\s]", "");		
		if(strAmount == "") {
			return isVerified;
		}
		
		double amount = Double.parseDouble(strAmount);
		int userId = Integer.parseInt(strUserId);
		
		if(rDao.insertNewReimbursement(amount, description, type, userId) == 201) {
			isVerified = true;
		}
		
		return isVerified;		
	}
	
	public boolean updateReim(int reimId, int userId, int reimStatus) {
		boolean isVerified = false;
		
		if(rDao.updateExistingReimbursement(reimId, userId, reimStatus) == 200) {
			isVerified = true;
		}
		
		return isVerified;
	}
	
	public boolean removeReim(int id) {
		boolean isVerified = false;
		
		if(rDao.deleteExistingReimbursement(id) == 201) {
			isVerified = true;
		}
		
		return isVerified;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
