package com.ers.service;

import com.ers.dao.UserDao;
import com.ers.model.User;

public class UserService {
	
	private UserDao uDao;
	
	public UserService() {
		
	}
	
	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}
	
//	Retrieves user information from the database
	public User getUserByName(String firstName, String lastName) {
		User user = uDao.findByName(firstName, lastName);
		
		if(user == null) {
			throw new NullPointerException();
		}
		
		return user;
	}
	
	public User getUserByUsername(String username) {
		User user = uDao.findByUsername(username);
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public User getUserByEmail(String email) {
		User user = uDao.findByEmail(email);
		
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
//	Creates a new user
	public boolean addNewUser(String username, String password, String firstName, String lastName, String email) {
		boolean isVerified = false;
		
		User user = new User(username, password, firstName, lastName, email, 2);
		
		if(uDao.insertNewUser(user) == 201) {
			isVerified = true;
		}
		
		return isVerified;
		
	}
	
//	Deletes an existing user
	public boolean deleteUser(User user) {
		
		boolean isVerified = false;
		
		if(uDao.removeUser(user.getUsername()) == 201) {
			isVerified = true;
		}
		
		return isVerified;
		
	}
	
//	Verify username before verifying passwords!
	public boolean passwordVerify(String username, String password) {
		boolean isVerified = false;
		User user = getUserByUsername(username);
		
		if(user.getPassword().equals(password)) {
			System.out.println("password confirmed...\n");
			isVerified = true;
		}
		System.out.println(isVerified);
		return isVerified;
	}

}
