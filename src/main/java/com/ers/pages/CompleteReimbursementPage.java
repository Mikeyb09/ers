package com.ers.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CompleteReimbursementPage {

	private WebDriver driver;
	private LoginPage loginPage;

	private List<WebElement> reimRow;
	private WebElement submitBtn;
	private WebElement updateBtn;
	private Select status;
	

	public CompleteReimbursementPage(WebDriver driver) {
		
		this.driver = driver;		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		this.navigateTo();		
//		this.reimRow = driver.findElements(By.tagName("tr"));
		
		openReim();		
		System.out.println("Should have called this function...");
		
		openUpdateWindow();		
		this.status = new Select(driver.findElement(By.name("status")));		
		this.submitBtn = driver.findElement(By.id("submitBtn"));
		
	}
	
	
	public void navigateTo() {
		
		loginPage = new LoginPage(driver);
		loginPage.setUsername("jacob.h");
		loginPage.setPassword("password");
		loginPage.submit();	
		
	}
	
	public void openReim() {
		System.out.println("Method was accessed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("row0")));
		
		this.reimRow = driver.findElements(By.id("row0"));
		this.reimRow.get(0).click();
		this.updateBtn = driver.findElement(By.id("updateBtn"));
	}
	
	public void openUpdateWindow() {
		this.updateBtn.click();
	}
	
	
	public void setStatus(String status) {
		this.status.selectByVisibleText(status);
	}
	
	
	public void submit() {
		this.submitBtn.click();
	}
	
	
	
	

}
