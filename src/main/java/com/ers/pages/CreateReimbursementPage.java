package com.ers.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CreateReimbursementPage {
	
	private WebDriver driver;
	private LoginPage loginPage;
	
	private WebElement reimBtn;
	private WebElement amountField;
	private Select typeField;
	private WebElement descriptionField;
	private WebElement submitBtn;
	
	
	public CreateReimbursementPage(WebDriver driver) {
		
		this.driver = driver;
		
		this.navigateTo();
		
		this.reimBtn = driver.findElement(By.id("newReimbursementBtn"));
		this.openReimForm();
		
		this.amountField = driver.findElement(By.id("amount"));
		this.typeField = new Select(driver.findElement(By.id("type")));
		this.descriptionField = driver.findElement(By.id("description"));
		this.submitBtn = driver.findElement(By.id("submitBtn"));
		
	}
	
	
	public void navigateTo() {
		
		loginPage = new LoginPage(driver);
		loginPage.setUsername("mikeyb09");
		loginPage.setPassword("password");
		loginPage.submit();		
		
	}
	
	public void openReimForm() {
		this.reimBtn.click();
	}
	
	public String getAmountField() {
		return this.amountField.getAttribute("value");
	}
	
	public void setAmountField(String amount) {
		this.amountField.clear();
		this.amountField.sendKeys(amount);
	}
	
	public void setTypeField(String type) {
		this.typeField.selectByVisibleText(type);
	}
	
	public String getDescriptionField() {
		return this.descriptionField.getAttribute("value");
	}
	
	public void setDescriptionField(String description) {
		this.descriptionField.clear();
		this.descriptionField.sendKeys(description);
	}
	
	public void submit() {
		this.submitBtn.click();
	}
	

}
