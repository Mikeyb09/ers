package com.ers.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	private WebDriver driver;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement submitBtn;
	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		
		this.navigateTo();
		
		this.usernameField = driver.findElement(By.id("username"));
		this.passwordField = driver.findElement(By.id("password"));
		this.submitBtn = driver.findElement(By.id("login-submit-btn"));
		
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9001/html/index.html");
		driver.manage().window().maximize();
	}
	
	public String getUsername() {
		return this.usernameField.getAttribute("value");
	}
	
	public void setUsername(String username) {
		this.usernameField.clear();
		this.usernameField.sendKeys(username);
	}
	
	public String getPassword() {
		return this.passwordField.getAttribute("value");
	}
	
	public void setPassword(String password) {
		this.passwordField.clear();
		this.passwordField.sendKeys(password);
	}
	
	public void submit() {
		this.submitBtn.click();
	}

}
