package com.ers.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ERSDBConnection {
	
	private String url = "jdbc:mariadb://database-1.cbz16tgahuca.us-east-2.rds.amazonaws.com:3306/ERS";
	private String username = "ERSUser";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}

}
