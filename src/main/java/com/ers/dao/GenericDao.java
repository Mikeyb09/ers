package com.ers.dao;

public interface GenericDao<E> {
	
	public E findByUsername(String username);
	public int insert(E entity);
	
}
