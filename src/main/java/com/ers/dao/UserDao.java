package com.ers.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ers.model.User;

public class UserDao implements GenericDao<User>{
	
	private ERSDBConnection ersCon;
	private DriverManager dm;
	
	public UserDao() {
		
	}
	
	public UserDao(ERSDBConnection ersCon) {
		this.ersCon = ersCon;
	}

	public User findByName(String fname, String lname) {
		// TODO Auto-generated method stub
		
		User user = null;
		
		try(Connection con = ersCon.getDBConnection()){
			
			String sql = "SELECT * FROM ERS_Users WHERE first_name = " + fname + " AND last_name = " + lname;
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return user;
			}
			
			user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public User findByEmail(String email) {
		
		User user = null;
		
		try(Connection con = ersCon.getDBConnection()){
			
			String sql = "SELECT * FROM ERS_Users WHERE email = '" + email + "';";
						
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return user;
			}
			
			user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
		
	}
	
	public int insertNewUser(User user) {
		
		int status = 404;
		
		try(Connection con = ersCon.getDBConnection()){
			
			String sql = "INSERT INTO ERS_Users (username, password, first_name, last_name, email, user_role_id) VALUES('" + user.getUsername() + "', '"
					+ user.getPassword() + "', '"
					+ user.getFirstName() + "', '"
					+ user.getLastName() + "', '"
					+ user.getEmail() + "', "
					+ user.getUserRole() + ")";
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.executeUpdate();
			
			status = 201;
			
		}catch(SQLException e) {
			e.printStackTrace();
		} 
		
		return status;
		
	}
	
	
	public int removeUser(String username) {
		
		int status = 404;
		
		try(Connection con = ersCon.getDBConnection()){
			
			String sql = "DELETE FROM ERS_Users WHERE username = '" + username + "';";
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			if(ps.executeUpdate() != 0) {
				status = 200;
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return status;
		
	}
	
	
//	Implements from GenericDao 
	
	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		
		User user = null;
		
		try(Connection con = ersCon.getDBConnection()){
			
			String sql = "SELECT * FROM ERS_Users WHERE username = '" + username + "';";
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			System.out.println("Checking Username");
			if(!rs.first()) {
				System.out.println("Did not find user by Username");
				return user;
			}
			System.out.println("Found user by Username");
			user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			
			System.out.println(user);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	@Override
	public int insert(User user) {
		// TODO Auto-generated method stub
		return 0;
	}


}
