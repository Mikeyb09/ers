package com.ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.ers.model.Reimbursement;

public class ReimbursementDao implements GenericDao<Reimbursement> {

	private ERSDBConnection ersCon;
	private final static Logger log = Logger.getLogger(ReimbursementDao.class);

	public ReimbursementDao() {

	}

	public ReimbursementDao(ERSDBConnection ersCon) {
		this.ersCon = ersCon;
	}
//	----------------------------
//	Retrieve reimbursements
//	----------------------------

	// Returns a list of reimbursements created by an author

	public List<Reimbursement> findAllReimbursements() {

		List<Reimbursement> reimList = new ArrayList<>();
		Reimbursement reim;
		int authorId, resolverId, statusId, typeId;
		
		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT * FROM ERS_Reimbursement";
			
			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {				
				
				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6));

				authorId = rs.getInt(7);
				resolverId = rs.getInt(8);
				statusId = rs.getInt(9);
				typeId = rs.getInt(10);

				reim.setReim_author(getUsersName(authorId));
				reim.setReim_resolver(getUsername(resolverId));
				reim.setReim_status(getStatus(statusId));
				reim.setReim_type(getType(typeId));

				reimList.add(reim);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimList;
	}

//	
	public Reimbursement findById(int id) {

		Reimbursement reim = null;
		int authorId, resolverId, statusId, typeId;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT * FROM ERS_Reimbursement WHERE reim_id = " + id;

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return reim;
			}

			reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4), rs.getString(5),
					rs.getString(6));

			authorId = rs.getInt(7);
			resolverId = rs.getInt(8);
			statusId = rs.getInt(9);
			typeId = rs.getInt(10);

			reim.setReim_author(getUsersName(authorId));
			reim.setReim_resolver(getUsername(resolverId));
			reim.setReim_status(getStatus(statusId));
			reim.setReim_type(getType(typeId));

			log.info(reim);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reim;
	}

	// Returns a list of reimbursements created by an author
//	public List<Reimbursement> findByAuthor(String author) {
//
//		List<Reimbursement> reimList = new ArrayList<>();
//		Reimbursement reim;
//		int resolverId, statusId, typeId, authorId;
//
//		try (Connection con = ersCon.getDBConnection()) {
//
//			String sql = "SELECT * FROM ERS_Reimbursement WHERE reim_author = " + getUserId(author);
//
//			PreparedStatement ps = con.prepareStatement(sql);
//
//			ResultSet rs = ps.executeQuery();
//
//			while (rs.next()) {
////				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
////						rs.getString(5), rs.getString(6));
//				
//				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), getUsersName(rs.getInt(7)), getUsername(rs.getInt(8)), getStatus(rs.getInt(9)), getType(rs.getInt(10)));
//
////				authorId = rs.getInt(7);
////				resolverId = rs.getInt(8);
////				statusId = rs.getInt(9);
////				typeId = rs.getInt(10);
////
////				reim.setReim_author(getUsersName(authorId));
////				reim.setReim_resolver(getUsername(resolverId));
////				reim.setReim_status(getStatus(statusId));
////				reim.setReim_type(getType(typeId));
//
//				reimList.add(reim);
//
//			}
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return reimList;
//	}

	public List<Reimbursement> findByAuthorAndStatus(int authorId, String status) {

		List<Reimbursement> reimList = new ArrayList<>();
		Reimbursement reim;
		int resolverId, statusId, typeId;

		try (Connection con = ersCon.getDBConnection()) {

			String sql;

			if (status == "Pending") {
				sql = "SELECT * FROM ERS_Reimbursement WHERE reim_author = " + authorId + " AND reim_status_id = "
						+ getStatusId(status);
			} else {
				sql = "SELECT * FROM ERS_Reimbursement WHERE reim_author = " + authorId + " AND reim_status_id != 3";
			}

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6));

				resolverId = rs.getInt(8);
				statusId = rs.getInt(9);
				typeId = rs.getInt(10);

				reim.setReim_author(getUsersName(authorId));
				reim.setReim_resolver(getUsername(resolverId));
				reim.setReim_status(getStatus(statusId));
				reim.setReim_type(getType(typeId));

				reimList.add(reim);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimList;
	}

	public List<Reimbursement> findByStatus(String status) {

		List<Reimbursement> reimList = new ArrayList<>();
		Reimbursement reim;
		int authorId, resolverId, statusId, typeId;

		try (Connection con = ersCon.getDBConnection()) {

			String sql;

			if (status == "Pending") {
				sql = "SELECT * FROM ERS_Reimbursement WHERE reim_status_id = " + getStatusId(status) + ";";
			} else {
				sql = "SELECT * FROM ERS_Reimbursement WHERE reim_status_id != 3;";
			}

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6));

				authorId = rs.getInt(7);
				resolverId = rs.getInt(8);
				statusId = rs.getInt(9);
				typeId = rs.getInt(10);

				reim.setReim_author(getUsersName(authorId));
				reim.setReim_resolver(getUsername(resolverId));
				reim.setReim_status(getStatus(statusId));
				reim.setReim_type(getType(typeId));

				reimList.add(reim);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimList;
	}

	// Returns a list of reimbursements resolved by a user
	public List<Reimbursement> findByResolver(String resolver) {

		List<Reimbursement> reimList = new ArrayList<>();
		Reimbursement reim;
		int authorId, resolverId, statusId, typeId;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT * FROM ERS_Reimbursement WHERE reim_resolver = '" + resolver + "';";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6));

				authorId = rs.getInt(7);
				resolverId = rs.getInt(8);
				statusId = rs.getInt(9);
				typeId = rs.getInt(10);

				reim.setReim_author(getUsersName(authorId));
				reim.setReim_resolver(getUsersName(resolverId));
				reim.setReim_status(getStatus(statusId));
				reim.setReim_type(getType(typeId));

				reimList.add(reim);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimList;
	}

	/*******************************************************************
	 * 
	 * General CREATE, UPDATE, and DELETE Methods
	 * 
	 *******************************************************************/

//	----------------------------
//	Create new reimbursement
//	----------------------------
	public int insertNewReimbursement(double amount, String description, String type, int userId) {

		int status = 404;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "INSERT INTO ERS_Reimbursement(reim_amount, reim_description, reim_submitted, reim_author, reim_status_id, reim_type_id) VALUES("
					+ amount + ", '" + description + "', '" + java.time.LocalDate.now() + " "
					+ java.time.LocalTime.now() + "', " + userId + ", " + "3, " + getTypeId(type) + ");";

			PreparedStatement ps = con.prepareStatement(sql);

			if (ps.executeUpdate() != 0) {
				status = 201;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return status;

	}

//	----------------------------
//	Updates a reimbursement
//	----------------------------
	public int updateExistingReimbursement(int reimId, int userId, int reimStatus) {

		int verified = 404;

		try (Connection con = ersCon.getDBConnection()) {

//			Reimbursement oldReim = findById(reimId);
//			String[] splitName = oldReim.getReim_author().split(",");
//			int authorId = getUserId(splitName[0].trim(), splitName[splitName.length - 1].trim());

			String sql = "UPDATE ERS_Reimbursement SET reim_status_id = " + reimStatus + ", " + "reim_resolved = '"
					+ java.time.LocalDate.now() + " " + java.time.LocalTime.now() + "', " + "reim_resolver = " + userId
					+ " WHERE reim_id = " + reimId;

			PreparedStatement ps = con.prepareStatement(sql);

			if (ps.executeUpdate() != 0) {
				
				verified = 200;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return verified;
	}

//	----------------------------
//	Deletes a reimbursement
//	----------------------------
	public int deleteExistingReimbursement(int id) {
		int status = 404;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "DELETE FROM ERS_Reimbursement WHERE reim_id = '" + id + "';";

			PreparedStatement ps = con.prepareStatement(sql);

			if (ps.executeUpdate() != 0) {
				status = 200;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return status;
	}

	/*******************************************************************
	 * 
	 * Implemented Methods to be overridden from the Generic Dao class
	 * 
	 *******************************************************************/
	@Override
	public Reimbursement findByUsername(String username) {
		// TODO Auto-generated method stub

		Reimbursement reim = null;
		
		try(Connection con = ersCon.getDBConnection()){
			
			String sql = "SELECT * FROM ERS_Reimbursement where reim_author = " + getUserId(username);
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return reim;
			}
			
			reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), getUsername(rs.getInt(7)), getUsername(rs.getInt(8)), getStatus(rs.getInt(9)), getType(rs.getInt(10)));			
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		return reim;
	}

	@Override
	public int insert(Reimbursement reim) {
		// TODO Auto-generated method stub
		int status = 404;
		return status;

	}

/*******************************************************************
 * 
 * Methods to populate fields with data queried from related tables.
 * 
 *******************************************************************/
	public String getUsername(int id) {

		String username = null;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT username FROM ERS_Users WHERE user_id = " + id;

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return username;
			}

			username = rs.getString(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return username;
	}

	public int getUserId(String username) {

		int id = 0;
//		log.info(username);
		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT user_id FROM ERS_Users WHERE username = '" + username + "';";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return id;
			}

			id = rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

//		log.info(id);
		return id;
	}

	public int getUserId(String lastName, String firstName) {

		int id = 0;
		log.info(lastName + " : " + firstName);
		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT user_id FROM ERS_Users WHERE last_name = '" + lastName + "' AND first_name = '"
					+ firstName + "';";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return id;
			}

			id = rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

//		log.info(id);
		return id;
	}

	public String getUsersName(int id) {

		String username = null;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT first_name, last_name FROM ERS_Users WHERE user_id = " + id;

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return username;
			}

			username = rs.getString(2) + ", " + rs.getString(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return username;
	}

	public String getStatus(int id) {

		String status = null;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT reimbursement_status FROM ERS_Reimbursement_Status WHERE reimbursement_status_id = "
					+ id;

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return status;
			}

			status = rs.getString(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return status;
	}

	public int getStatusId(String status) {

		int id = 0;
//		log.info(status);
		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT reimbursement_status_id FROM ERS_Reimbursement_Status WHERE reimbursement_status = '"
					+ status + "';";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return id;
			}

			id = rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

//		log.info(id);
		return id;
	}

	public String getType(int id) {

		String type = null;

		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT reimbursement_type FROM ERS_Reimbursement_Type WHERE reimbursement_type_id = " + id;

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return type;
			}

			type = rs.getString(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return type;
	}

	public int getTypeId(String type) {

		int id = 0;
		log.info(type);
		try (Connection con = ersCon.getDBConnection()) {

			String sql = "SELECT reimbursement_type_id FROM ERS_Reimbursement_Type WHERE reimbursement_type = '" + type
					+ "';";

			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return id;
			}

			id = rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

//		log.info(id);
		return id;
	}

}
