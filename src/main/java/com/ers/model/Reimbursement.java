package com.ers.model;

public class Reimbursement {
	
	private int reim_Id;
	private double reim_amount;
	private String reim_submitted;
	private String reim_resolved;
	private String reim_description;
	private String reim_receipt;
	private String reim_author;
	private String reim_resolver;
	private String reim_status;
	private String reim_type;
	
	public Reimbursement() {
		
	}
	
//	Used for setting up a brand new reimbursement object that has not been submitted or resolved!
	public Reimbursement(double reim_amount, String reim_submitted, String reim_description, String reim_author, String reim_status, String reim_type) {
		super();
		this.reim_amount = reim_amount;
		this.reim_submitted = reim_submitted;
		this.reim_description = reim_description;
		this.reim_author = reim_author;
		this.reim_status = reim_status;
		this.reim_type = reim_type;
	}	
	
	public Reimbursement(double reim_amount, String reim_author, String reim_status, String reim_type) {
		super();
		this.reim_amount = reim_amount;
		this.reim_author = reim_author;
		this.reim_status = reim_status;
		this.reim_type = reim_type;
	}
	
	public Reimbursement(double reim_amount, String reim_submitted, String reim_author, String reim_status, String reim_type) {
		super();
		this.reim_amount = reim_amount;
		this.reim_submitted = reim_submitted;
		this.reim_author = reim_author;
		this.reim_status = reim_status;
		this.reim_type = reim_type;
	}
	
	public Reimbursement(int reim_Id, double reim_amount, String reim_submitted, String reim_resolved,
			String reim_description, String reim_receipt) {
		super();
		this.reim_Id = reim_Id;
		this.reim_amount = reim_amount;
		this.reim_submitted = reim_submitted;
		this.reim_resolved = reim_resolved;
		this.reim_description = reim_description;
		this.reim_receipt = reim_receipt;
	}	

//	USED FOR DAO TESTING
	public Reimbursement(int reim_Id, double reim_amount, String reim_submitted, String reim_resolved,
			String reim_description, String reim_author, String reim_resolver, String reim_status, String reim_type) {
		super();
		this.reim_Id = reim_Id;
		this.reim_amount = reim_amount;
		this.reim_submitted = reim_submitted;
		this.reim_resolved = reim_resolved;
		this.reim_description = reim_description;
		this.reim_author = reim_author;
		this.reim_resolver = reim_resolver;
		this.reim_status = reim_status;
		this.reim_type = reim_type;
	}
	
	public Reimbursement(int reim_Id, double reim_amount, String reim_submitted, String reim_resolved,
			String reim_description, String reim_receipt, String reim_author, String reim_resolver, String reim_status, String reim_type) {
		super();
		this.reim_Id = reim_Id;
		this.reim_amount = reim_amount;
		this.reim_submitted = reim_submitted;
		this.reim_resolved = reim_resolved;
		this.reim_description = reim_description;
		this.reim_author = reim_author;
		this.reim_resolver = reim_resolver;
		this.reim_status = reim_status;
		this.reim_type = reim_type;
		this.reim_receipt = reim_receipt;
	}
	
	

	public int getReim_Id() {
		return reim_Id;
	}

	public double getReim_amount() {
		return reim_amount;
	}

	public void setReim_amount(double reim_amount) {
		this.reim_amount = reim_amount;
	}

	public String getReim_submitted() {
		return reim_submitted;
	}

	public void setReim_submitted(String reim_submitted) {
		this.reim_submitted = reim_submitted;
	}

	public String getReim_resolved() {
		return reim_resolved;
	}

	public void setReim_resolved(String reim_resolved) {
		this.reim_resolved = reim_resolved;
	}

	public String getReim_description() {
		return reim_description;
	}

	public void setReim_description(String reim_description) {
		this.reim_description = reim_description;
	}

	public String getReim_receipt() {
		return reim_receipt;
	}

	public void setReim_receipt(String reim_receipt) {
		this.reim_receipt = reim_receipt;
	}

	public String getReim_author() {
		return reim_author;
	}

	public void setReim_author(String reim_author) {
		this.reim_author = reim_author;
	}

	public String getReim_resolver() {
		return reim_resolver;
	}

	public void setReim_resolver(String reim_resolver) {
		this.reim_resolver = reim_resolver;
	}
	
	public String getReim_status() {
		return reim_status;
	}

	public void setReim_status(String reim_status) {
		this.reim_status = reim_status;
	}
	
	public String getReim_type() {
		return reim_type;
	}

	public void setReim_type(String reim_type) {
		this.reim_type = reim_type;
	}
	
	
	
	
	@Override
	public String toString() {
		return "Reimbursement [reim_Id=" + reim_Id + ", reim_amount=" + reim_amount + ", reim_submitted="
				+ reim_submitted + ", reim_resolved=" + reim_resolved + ", reim_description=" + reim_description
				+ ", reim_receipt=" + reim_receipt + ", reim_author=" + reim_author + ", reim_resolver=" + reim_resolver
				+ ", reim_status=" + reim_status + ", reim_type=" + reim_type + "]";
	}
	
	

}
