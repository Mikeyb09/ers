package com.ers;

import com.ers.controller.ReimbursementController;
import com.ers.controller.UserController;
import com.ers.dao.ERSDBConnection;
import com.ers.dao.ReimbursementDao;
import com.ers.dao.UserDao;
import com.ers.model.User;
import com.ers.service.ReimbursementService;
import com.ers.service.UserService;

import io.javalin.Javalin;

public class MainDriver {
	
	public static void main(String[] args) {	
		
		UserController uCon = new UserController(new UserService(new UserDao(new ERSDBConnection())));
		ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new ERSDBConnection())));
		
		
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
			config.enableCorsForAllOrigins();
		});
		
		app.start(9001);
		
		
//		Use before to control what users are allowed to access
		app.before("/html/userHome.html", (ctx) -> {
			
			if(ctx.sessionAttribute("userInfo") == null) {
				ctx.redirect("/html/index.html");			
			} else {
				User user = ctx.sessionAttribute("userInfo");
				
				if(user.getUserRole() == 1) {
					ctx.redirect("/html/forbidden.html");
				}
			}
			
		});
		
		app.before("/html/managerHome.html", (ctx) -> {
			
			if(ctx.sessionAttribute("userInfo") == null) {
				ctx.redirect("/html/index.html");			
			} else {
				User user = ctx.sessionAttribute("userInfo");
				
				if(user.getUserRole() == 2) {
					ctx.redirect("/html/forbidden.html");
				}
			}
			
		});
		
		app.before("/html/index.html", (ctx) -> {
			
			if(ctx.sessionAttribute("userInfo") != null) {
				User user = ctx.sessionAttribute("userInfo");
				
				if(user.getUserRole() == 1) {
					ctx.redirect("/html/managerHome.html");
				} else if(user.getUserRole() == 2) {
					ctx.redirect("/html/userHome.html");
				}
			}			
		});
		
		
		
		app.before("/html/logout.html", uCon.logout);
		
		
		
		
		app.get("/", uCon.getLogin);
		
		app.get("/ers/login", uCon.getLogin);
		
		app.post("/ers/login", uCon.postLogin);
		
		app.get("/html/logout", uCon.logout);
		
		app.get("/ers/error", (ctx) -> {
			ctx.redirect("/html/error.html");
		});
		
		
		app.get("/ers/managerHome", uCon.getLogin);	
		
		app.post("/ers/managerHome/register", uCon.newUser);	
		
		app.get("/ers/managerHome/completed", rCon.managerHomeCompleted);
		
		app.get("/ers/managerHome/uncompleted", rCon.managerHomeUncompleted);
		
		app.get("/ers/managerHome/reimbursement/:reimId", rCon.viewSingleReimbursement);		

		app.post("/ers/managerHome/reimbursement/:reimId", rCon.submitReimChanges);
		
		
//		app.get("/ers/userHome/reimbursements/", rCon.userHomeReimbursement);
		
		app.post("/ers/userHome/newReimbursement/", rCon.createNewReimbursement);
		
		app.get("/ers/userHome/completed", rCon.userHomeCompleted);
		
		app.get("/ers/userHome/uncompleted", rCon.userHomeUncompleted);
		
		
		
		
		app.exception(NullPointerException.class, (e, ctx) ->{
			ctx.status(404);
			ctx.result("User does not exist");
			ctx.redirect("/html/failed-login.html");
		});
		
		app.exception(Exception.class, (e, ctx) -> {
			e.printStackTrace();
			ctx.redirect("/html/error.html");
		});
		
		
		
	}

}
