package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ers.dao.ReimbursementDao;
import com.ers.model.Reimbursement;
import com.ers.service.ReimbursementService;

public class ReimServiceTest {

	@Mock
	private ReimbursementDao mockDao;

	private ReimbursementService testService = new ReimbursementService(mockDao);

	private Reimbursement testReim, testReim2;

	private List<Reimbursement> reimList = new ArrayList<>();
	private List<Reimbursement> pendingList = new ArrayList<>();
	private List<Reimbursement> completedList = new ArrayList<>();
	private List<Reimbursement> authorList = new ArrayList<>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockDao);

		testReim = new Reimbursement(1, 100.25, "1/2/2021", "1/3/2021", "reimbursement for hotel", null, "mikeyb09",
				"jacob.h", "Pending", "Hotel");
		Reimbursement testReim2 = new Reimbursement(2, 12.25, "1/4/2021", "1/5/2021", "reimbursement for food", null,
				"mikeyb09", "jacob.h", "Approved", "Food");

		reimList.add(testReim);
		reimList.add(testReim2);
		pendingList.add(testReim);
		completedList.add(testReim2);

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testGetReimById() {

		when(mockDao.findById(Mockito.anyInt())).thenReturn(testReim);

		assertEquals(testReim, testService.getReimById(1));

	}

	@Test
	public void testGetReimByIdFail() {

		when(mockDao.findById(Mockito.anyInt())).thenReturn(testReim);

		assertNotEquals(testReim2, testService.getReimById(1));
	}

	@Test
	public void testGetAllReims() {

		when(mockDao.findAllReimbursements()).thenReturn(reimList);

		assertEquals(reimList.size(), testService.getAllReims().size());

	}

	@Test
	public void testGetAllReimsFail() {

		when(mockDao.findAllReimbursements()).thenReturn(reimList);

		assertNotEquals(0, testService.getAllReims().size());

	}

	@Test
	public void testGetAllPendingReims() {

		when(mockDao.findByStatus("Pending")).thenReturn(pendingList);

		assertEquals(pendingList, testService.getAllPendingReims());
	}

	@Test
	public void testGetAllPendingReimsFail() {

		when(mockDao.findByStatus("Pending")).thenReturn(pendingList);

		assertNotEquals(null, testService.getAllPendingReims());
	}

	@Test
	public void testGetAllCompletedReims() {

		when(mockDao.findByStatus("")).thenReturn(completedList);

		assertEquals(completedList, testService.getAllCompletedReims());
	}

	@Test
	public void testFindByAuthorAndStatus() {

		when(mockDao.findByAuthorAndStatus(10000, "Pending")).thenReturn(pendingList);

		assertEquals(pendingList.size(), testService.getAuthorPendingReim("10000").size());
	}

	@Test
	public void testGetAuthorCompletedReim() {

		when(mockDao.findByAuthorAndStatus(10000, "")).thenReturn(completedList);

		assertEquals(completedList.size(), testService.getAuthorCompletedReim("10000").size());
	}

	@Test
	public void testGetReimByResolver() {

		when(mockDao.findByResolver(Mockito.anyString())).thenReturn(reimList);

		assertEquals(reimList, testService.getReimByResolver("mikeyb09"));
	}

	@Test
	public void testCreateNewReim() {

		when(mockDao.insertNewReimbursement(Mockito.anyDouble(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(201);

		assertEquals(true, testService.createNewReim("150.00", "business travel", "Travel", "10000"));

	}

	@Test
	public void testCreateNewReimFail() {

		when(mockDao.insertNewReimbursement(Mockito.anyDouble(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(201);

		assertNotEquals(false, testService.createNewReim("150.00", "business travel", "Travel", "10000"));

	}
	
	
	@Test
	public void testUpdateReim() {
		
		when(mockDao.updateExistingReimbursement(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(200);
		
		assertEquals(true, testService.updateReim(1, 10000, 1));
	}
	
	@Test
	public void testUpdateReimFail() {
		
		when(mockDao.updateExistingReimbursement(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(200);
		
		assertNotEquals(false, testService.updateReim(1, 10000, 1));
	}
	
	@Test
	public void testRemoveReim() {
		
		when(mockDao.deleteExistingReimbursement(Mockito.anyInt())).thenReturn(201);
		
		assertEquals(true, testService.removeReim(1));
	}
	
	@Test
	public void testRemoveReimFail() {
		
		when(mockDao.deleteExistingReimbursement(Mockito.anyInt())).thenReturn(201);
		
		assertNotEquals(false, testService.removeReim(1));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
