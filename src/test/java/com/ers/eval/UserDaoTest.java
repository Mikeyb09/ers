package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ers.dao.ERSDBConnection;
import com.ers.dao.UserDao;
import com.ers.model.User;

public class UserDaoTest {
	
//	Set up mocks
//	data source is something that is given to our driver manager, it is our connection object
	@Mock
	private ERSDBConnection ersdb;
	
//	mocks our connection
	@Mock
	private Connection con;
	
//	Mock prepared statement and result set
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private UserDao testDao;
	
//	Does not need to be mocked because it is the model object that will be set up for testing
	private User testUser;
	
	
	@BeforeClass
	public static void setUpBeforeClass() {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		
	}
	
	@Before
	public void setUp() throws Exception{
		//initializing the mocked objects. this method will initialize the object by calling their constructors and so on.

		MockitoAnnotations.initMocks(this);
		when(ersdb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(any(String.class))).thenReturn(ps);
		
		testUser = new User("mikeyb09", "password", "mikey", "barlowe", "m_barlowe@hotmail.com", 2);
		
//		FOR TESTING DAO use rs.first() OR rs.next()
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getUserId());
		when(rs.getString(2)).thenReturn(testUser.getUsername());
		when(rs.getString(3)).thenReturn(testUser.getPassword());
		when(rs.getString(4)).thenReturn(testUser.getFirstName());
		when(rs.getString(5)).thenReturn(testUser.getLastName());
		when(rs.getString(6)).thenReturn(testUser.getEmail());
		when(rs.getInt(7)).thenReturn(testUser.getUserRole());
		when(ps.executeQuery()).thenReturn(rs);
	}
	
	@After
	public void tearDown() {
		
	}
	
	
//	Tests for findByUsername in UserDao
	@Test
	public void testFindByUsernameSuccess() {
		System.out.println(testUser.getUsername());
		System.out.println("\n" + new UserDao(ersdb).findByUsername("mikeyb09").getUsername());
		assertEquals(testUser.getUsername(), new UserDao(ersdb).findByUsername("mikeyb09").getUsername());
	}
	
	@Test
	public void testFindByUsernameFailure() {
		assertNotEquals("asdf", testUser.getUsername());
	}
	
//	Tests for findByEmail in UserDao
	@Test
	public void testFindByEmailSuccess() {
		assertEquals(new UserDao(ersdb).findByEmail("m_barlowe@hotmail.com").getEmail(), testUser.getEmail());
	}
	
	@Test
	public void testFindByEmailFailure() {
		assertNotEquals("asdf", testUser.getEmail());
	}
	
//	Tests for Dao insert method
	@Test
	public void testInsertNewUser() {
		testDao.insertNewUser(testUser);
		
		verify(testDao, times(1)).insertNewUser(testUser);
	}
	
//	Test for Dao remove method
//	@Test
//	public void testRemoveUserSuccess() {
//		testDao.removeUser(testUser.getUsername());
//		verify(testDao, times(1)).removeUser(testUser.getUsername());
//	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
