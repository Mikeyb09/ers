package com.ers.eval.selenium;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ers.pages.CreateReimbursementPage;

public class CreateReimbursementTest {

	private static WebDriver driver;
	private CreateReimbursementPage page;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/msedgedriver.exe";
		System.setProperty("webdriver.edge.driver", filePath);

		driver = new EdgeDriver();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}

	@Before
	public void setUp() throws Exception {
		this.page = new CreateReimbursementPage(driver);
	}

	@After
	public void tearDown() throws Exception {

	}

	
	
	@Test
	public void createReimbursement() {
		
		page.setAmountField("100.25");
		page.setTypeField("Other");
		page.setDescriptionField("Some dummy text.");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/html/userHome.html"));
		assertEquals("http://localhost:9001/html/userHome.html", driver.getCurrentUrl());

	} 
	
	
	
	
	
	
	
	
	
}
