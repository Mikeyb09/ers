package com.ers.eval.selenium;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ers.pages.LoginPage;

public class LoginPageTest {
	
	private LoginPage page;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		String filePath = "src/test/resources/msedgedriver.exe";
		System.setProperty("webdriver.edge.driver", filePath);
		
		driver = new EdgeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception{
		this.page = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testUserSuccessfulLogin() {
		
		page.setUsername("mikeyb09");
		page.setPassword("password");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("html/userHome.html"));
		assertEquals("http://localhost:9001/html/userHome.html", driver.getCurrentUrl());
	}
	
	
	
	@Test
	public void testUserFailedLogin() {
		
		page.setUsername("tom89");
		page.setPassword("noWorky");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/failed-login.html"));
		assertEquals("http://localhost:9001/html/failed-login.html", driver.getCurrentUrl());
	}
	

}
