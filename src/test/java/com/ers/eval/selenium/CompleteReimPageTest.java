package com.ers.eval.selenium;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ers.pages.CompleteReimbursementPage;

public class CompleteReimPageTest {

	private static WebDriver driver;
	private CompleteReimbursementPage page;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/msedgedriver.exe";
		System.setProperty("webdriver.edge.driver", filePath);

		driver = new EdgeDriver();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}

	@Before
	public void setUp() throws Exception {
		this.page = new CompleteReimbursementPage(driver);
	}

	@After
	public void tearDown() throws Exception {

	}
	
	
	@Test
	public void updateReimbursement() {
		
		page.setStatus("Approved");
		page.submit();
		
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/html/managerHome.html"));
		assertEquals("http://localhost:9001/html/managerHome.html", driver.getCurrentUrl());

	} 

}
