package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ers.dao.ERSDBConnection;
import com.ers.dao.ReimbursementDao;
import com.ers.model.Reimbursement;

public class ReimDaoTest {
	
	@Mock
	private ERSDBConnection ersdb;
	
	@Mock
	private Connection con;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
//	@Mock
	private ReimbursementDao reimDao;

	private List<Reimbursement> mockList;
	
	private Reimbursement testReim;
	
	private final Logger log = Logger.getLogger(ReimDaoTest.class);
	
	
	
	@BeforeClass
	public static void setUpBeforeClass() {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		
	}
	
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		
		when(ersdb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(any(String.class))).thenReturn(ps);	
		
		testReim = new Reimbursement(1, 100.25, "1/2/2021", "1/3/2021", "reimbursement for hotel", null, "mikeyb09", "jacob.h", "Pending", "Hotel");
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(testReim.getReim_Id());
		when(rs.getDouble(2)).thenReturn(testReim.getReim_amount());
		when(rs.getString(3)).thenReturn(testReim.getReim_submitted());
		when(rs.getString(4)).thenReturn(testReim.getReim_resolved());
		when(rs.getString(5)).thenReturn(testReim.getReim_description());
		when(rs.getString(6)).thenReturn(testReim.getReim_receipt());
		when(rs.getInt(7)).thenReturn(10000/*testReim.getReim_author()*/);
		when(rs.getString(8)).thenReturn(testReim.getReim_resolver());
		when(rs.getString(9)).thenReturn(testReim.getReim_status());
		when(rs.getString(10)).thenReturn(testReim.getReim_type());
		
		when(ps.executeQuery()).thenReturn(rs);		
		when(ps.executeUpdate()).thenReturn(200);
	}
	
	@After
	public void tearDown() {
		
	}
	
	
/*
 * findAllReimbursements Success and Fail tests. tests are returned a list and the sizes are compared for verification. if the list contains objects, then the tests pass.
*/
	@Test
	public void testFindAllReimbursementsSuccess() {
		
//		Returns the size of the list
		assertEquals(1, new ReimbursementDao(ersdb).findAllReimbursements().size());		
	}
	
	@Test
	public void testFindAllReimbursementsFail() {
		assertNotEquals(2, new ReimbursementDao(ersdb).findAllReimbursements().size());
	}

/*
 * findById Success and Fail tests. an object with an ID is returned, the ID's are compared to ensure objects are returning with data.. 
 * */
	@Test
	public void testFindByIdSuccess() throws Exception{
		
		when(rs.first()).thenReturn(true);
		
		assertEquals(testReim.getReim_Id(), new ReimbursementDao(ersdb).findById(1).getReim_Id());
	}
	
	@Test
	public void testFindByIdFail() throws Exception{
		
		when(rs.first()).thenReturn(true);
				
		assertNotEquals(2, new ReimbursementDao(ersdb).findById(1).getReim_Id());		
	}
	
	
/*
 * 
 * */
	
	@Test
	public void testFindByAuthorAndStatusSuccess() throws Exception {
		
		
		
	}
	
	@Test
	public void testFindByAuthorAndStatusFail() throws Exception {
		
		
		
	}
	
	@Test
	public void testFindByStatusSuccess() throws Exception {
		
		
		
	}
	
	@Test
	public void testFindByStatusFail() throws Exception {
		
		
		
	}
	
	@Test
	public void testFindByResolverSuccess() throws Exception{
		
		
		
	}
	
	@Test
	public void testFindByResolverFail() throws Exception{
		
		
		
	}
	
	
//	NEEDS WORK
	@Test
	public void testUpdateReimbursement() throws Exception {
		
		assertEquals(200, new ReimbursementDao(ersdb).updateExistingReimbursement(testReim.getReim_Id(), 10001, 1));
	}

	@Test
	public void testUpdateReimbursementFail() throws Exception {
		
		assertNotEquals(404, new ReimbursementDao(ersdb).updateExistingReimbursement(testReim.getReim_Id(), 10001, 1));
	}
	
	@Test
	public void testDeleteExistingReimbursement() {
		
		assertEquals(200, new ReimbursementDao(ersdb).deleteExistingReimbursement(1));
	}
	
	@Test
	public void testDeleteExistingReimbursementFail() {
		
		assertNotEquals(404, new ReimbursementDao(ersdb).deleteExistingReimbursement(1));
	}
	
	
	
//	THIS ISNT GOING TO WORK WITH HOW YOU HAVE YOUR CLASSES SET UP!
	@Test
	public void testGetUsersName() throws Exception {
		
		when(rs.first()).thenReturn(true);
		when(reimDao.getUsersName(10000)).thenReturn(testReim.getReim_author());
		
		assertEquals(testReim.getReim_author(), new ReimbursementDao(ersdb).getUsersName(10000));
	}
	

	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
