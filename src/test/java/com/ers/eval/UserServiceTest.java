package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ers.dao.UserDao;
import com.ers.model.User;
import com.ers.service.UserService;

public class UserServiceTest {
	
	/*
	 * SERVICE TESTING
	 * 
	 * This section contains tests for the service classes
	 * 
	 * */
	// decalre a mocked user dao object
	@Mock
	private UserDao mockUser;
	// declare an instance of your user service class, and user dao class.
	static UserService uServ;
	// declare an instance of the user model
	private User testUser;
	
	
	@BeforeClass
	public static void setUpBeforeClass() {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		
	}
	
	@Before
	public void setUp() {
		//initializing the mocked objects. this method will initialize the object by calling their constructors and so on.
		MockitoAnnotations.initMocks(this);
		
//		assign the user service and inject the mocked user
		uServ = new UserService(mockUser);
		
//		creates the user object you want returned. use a member that already exists in the DB
//		you can also use members not in your DB *Test this theory*
		testUser = new User("mikeyb09", "password", "mikey", "barlowe", "m_barlowe@hotmail.com", 2);
		
//		when this method in this object is called, return the test object.
//		This will mock the DB rather than access the DB.
		when(mockUser.findByUsername("mikeyb09")).thenReturn(testUser);
		
//		 DO NOT ACCESS THE DATABASE FROM THIS CLASS!!!
//		uServ = new UserService(new UserDao(new ERSDBConnection()));
	}
	
	@After
	public void tearDown() {
		
	}
		
//	get by username tests
	@Test
	public void testGetByUsername() {
		assertEquals(uServ.getUserByUsername("mikeyb09"), testUser);
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetByUsernameFailure() {
		assertEquals(uServ.getUserByUsername("sfdgsdfg"), null);
	}	
	
//	get by email tests
	@Test
	public void testGetUserByEmailSuccess() {
		when(mockUser.findByEmail(Mockito.anyString())).thenReturn(testUser);
		
		assertEquals(uServ.getUserByEmail("m_barlowe@hotmail.com"), testUser);
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetByEmailFailure() {
		assertEquals(uServ.getUserByEmail("asdf"), testUser);
	}
	
//  Password Verification tests
	@Test
	public void testPasswordVerifySuccess() {
		assertTrue(uServ.passwordVerify("mikeyb09", "password"));
	}
	
	@Test
	public void testPasswordVerifyFailure() {
		assertFalse(uServ.passwordVerify("mikeyb09", "myPassword"));
	}
	
	@Test(expected = NullPointerException.class)
	public void testPasswordVerifyNullFailure() {
		assertFalse(uServ.passwordVerify("asdf", "myPassword"));
	}	

//	adding new user tests
//	@Test
//	public void testAddNewUser() {
//		uServ.addNewUser(testUser);
//		verify(mockUser, times(1)).insertNewUser(testUser);
//	}
	
//	deleting existing user tests
	
	@Test
	public void testDeleteUser() {
		uServ.deleteUser(testUser);
		verify(mockUser, times(1)).removeUser(testUser.getUsername());
	}

}


















