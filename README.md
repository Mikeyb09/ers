ERS - Employee Reimbursement System

Project Description

The Employee Reimbursement System is a web application built suing Java to proccess reimbursement requests from employee. Employees can log in using their account credentials and submit new requests, check the status of a already submitted requests, and review approvals/denials. The requests are managed by Financial managers who, when logged in, can see a list of all current pending requests as well as approve and deny those requests. Financial managers can also review requests that were completed by that manager.

Technolgies Used
- Java 8
- HTML
- CSS
- Bootstrap 4.5
- JavaScript
- AJAX
- Servlets
- JDBC
- SQL
- Maven
- Git 
- JUnit
- Log4j

Features
- Login/Logout
- Submit reimbursement requests
- Approve/Deny requests
- Request table organizing
- Account enrollment

TODO
- Password resets
- Email alerts

Getting Started

In order to try this project out for yourself, follow these instructions

1. Be sure to have Java 8 runtime environment installed.
2. Use the link to clone the repository:
    https://gitlab.com/Mikeyb09/ers.git
3. Open a browser and navigate to localhost:9001/
