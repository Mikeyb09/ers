window.onload = function() {
	console.log('window loaded');

	viewCompletedReim = 'true';
	changeView();

	document.getElementById('reimbursement-btn').addEventListener('click', changeView);

	let welcomeUser = document.getElementById('welcoming');
	welcomeUser.innerText = `Welcome ${getName()}`;
	
	sortTable(6);
}


function getReimbursementsOnLoad() {
	console.log('getting reimbursements');

	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {

			let reim = JSON.parse(xhttp.responseText);

			buildTable(reim);
		}
	}

	xhttp.open("GET", "http://localhost:9001/ers/managerHome/uncompleted");

	xhttp.send();
}


function verifyValues(temp) {
	if (temp.reim_description === null) {
		temp.reim_description = "---";
	}

	if (temp.receipt == null) {
		temp.reim_receipt = "---";
	}

	if (temp.reim_resolver === null) {
		temp.reim_resolver = "---";
	}

	if (!viewCompletedReim) {
		temp.reim_resolved = "---";
	}

	return temp;
}

function selectRow(id) {
	let xhttp = new XMLHttpRequest();
	
	document.getElementById("container-heading").innerText = "Loading...";
	let spinner = document.getElementById('loading-spinner');
	spinner.toggleAttribute("hidden");

	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {

			
			//get table body, set class to be unclickable, empty current rows, insert new row.
			let table = document.getElementById("table-body");
			table.className = '';

			table.innerHTML = '';

			let reim = JSON.parse(xhttp.responseText);

			let temp = verifyValues(reim);

			//Update the container-heading with reimbursement id number
			document.getElementById('container-heading').innerText = `Reimbursement # ${temp.reim_Id}`;
			spinner.toggleAttribute("hidden");

			let row = table.insertRow(0);

			let cell1 = row.insertCell(0);
			let cell2 = row.insertCell(1);
			let cell3 = row.insertCell(2);
			let cell4 = row.insertCell(3);
			let cell5 = row.insertCell(4);
			let cell6 = row.insertCell(5);
			let cell7 = row.insertCell(6);
			let cell8 = row.insertCell(7);
			let cell9 = row.insertCell(8);
			let cell10 = row.insertCell(9);

			cell1.innerHTML = temp.reim_Id;
			cell2.innerHTML = '$' + temp.reim_amount;
			cell3.innerHTML = temp.reim_type;
			cell4.innerHTML = temp.reim_status;
			cell5.innerHTML = temp.reim_description;
			cell6.innerHTML = temp.reim_author;
			cell7.innerHTML = temp.reim_submitted;
			cell8.innerHTML = temp.reim_resolved;
			cell9.innerHTML = temp.reim_receipt;
			cell10.innerHTML = `<button id="updateBtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#completeReim" data-id=${id}>`
				+ 'Update'
				+ '</button >';
		}
	}

	xhttp.open("GET", `http://localhost:9001/ers/managerHome/reimbursement/${id}`);

	xhttp.send();

}

function buildTable(reim) {

	let table = document.getElementById("table-body");

	table.innerHTML = '';

	for (let i = 0; i < reim.length; i++) {

		let temp = verifyValues(reim[i]);

		let row = table.insertRow(i);

		if (!viewCompletedReim) {
			row.setAttribute('onclick', `selectRow(${temp.reim_Id})`);
			row.setAttribute('id', `row${i}`);
			table.className = 'clickable';
		} else {
			table.className = '';
		}

		let cell1 = row.insertCell(0);
		let cell2 = row.insertCell(1);
		let cell3 = row.insertCell(2);
		let cell4 = row.insertCell(3);
		let cell5 = row.insertCell(4);
		let cell6 = row.insertCell(5);
		let cell7 = row.insertCell(6);
		let cell8 = row.insertCell(7);
		let cell9 = row.insertCell(8);

		cell1.innerHTML = temp.reim_Id;
		cell2.innerHTML = '$' + temp.reim_amount;
		cell3.innerHTML = temp.reim_type;
		cell4.innerHTML = temp.reim_status;
		cell5.innerHTML = temp.reim_description;
		cell6.innerHTML = temp.reim_author;
		cell7.innerHTML = temp.reim_submitted;
		cell8.innerHTML = temp.reim_resolved;
		cell9.innerHTML = temp.reim_receipt;

	}

}


function changeView() {

	console.log('Updating table');

	let btn = document.getElementById('reimbursement-btn');
	document.getElementById("container-heading").innerText = "Loading...";
	let spinner = document.getElementById('loading-spinner');

	//btn.innerText = "Loading...";
	spinner.toggleAttribute("hidden");

	viewCompletedReim = !viewCompletedReim

	// set table to show open reimbursements
	if (!viewCompletedReim) {

		console.log('Updating reimbursements');

		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {

			if (xhttp.readyState == 4 && xhttp.status == 200) {

				let reim = JSON.parse(xhttp.responseText);

				document.getElementById('container-heading').innerText = 'Open Reimbursements';
				btn.innerText = "View Completed Reimbursements";
				spinner.toggleAttribute("hidden");

				buildTable(reim);
			}
		}

		xhttp.open("GET", "http://localhost:9001/ers/managerHome/uncompleted");

		xhttp.send();

	} else {
		//set table to show closed reimbursements
		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {

			if (xhttp.readyState == 4 && xhttp.status == 200) {

				let reim = JSON.parse(xhttp.responseText);

				document.getElementById('container-heading').innerText = 'Completed Reimbursements';
				document.getElementById('table-body').innerHTML = '';

				btn.innerText = "View Open Reimbursements";
				spinner.toggleAttribute("hidden");

				buildTable(reim);
			}
		}

		xhttp.open("GET", "http://localhost:9001/ers/managerHome/completed");

		xhttp.send();
	}

}


$('#completeReim').on('show.bs.modal', function(event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var reimId = button.data('id') // Extract info from data-* attributes
	var username = getCookieName('username')
	var userId = getCookieName('userId')

	// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

	var modal = $(this)
	modal.find('.modal-title').text('Reimbursement #' + reimId)
	modal.find('#updateForm').attr("action", '/ers/managerHome/reimbursement/' + reimId)
	modal.find('.modal-body #managerId').attr('value', userId)
	modal.find('.modal-body #managerUsername').attr('value', username)
})


$('#newEnrollment').on('show.bs.modal', function(event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var recipient = button.data('whatever') // Extract info from data-* attributes

	// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

	var modal = $(this)
	modal.find('.modal-title').text('Employee Enrollment Form')
	modal.find('#newUserForm').attr("action", '/ers/managerHome/register')
	modal.find('.modal-body input').val(recipient)
})
