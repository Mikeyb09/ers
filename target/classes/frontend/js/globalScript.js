/**
 * 
 */

function getName() {

	let name = getCookieName('firstName');
	name.toLowerCase();
	let first = name.charAt(0).toUpperCase();
	name = first + name.slice(1);

	return name;
}

function getCookieName(name) {
	let result = document.cookie.match("(^|[^;]+)\\s*" + name + "\\s*=\\s*([^;]+)")
	return result ? result.pop() : ""
}

function signout() {
	console.log('logging out...');
	window.location.href = '/html/logout.html';
}


function sortTable(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchCount = 0;
	table = document.getElementById("reim-table");
	switching = true;
	
	dir = "asc";

	while (switching) {

		switching = false;
		rows = table.rows;

		for (i = 1; i < (rows.length - 1); i++) {

			shouldSwitch = false;

			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];

			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {

					shouldSwitch = true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {

					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {

			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;

			switchCount++;
		} else {

			if (switchCount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}



















